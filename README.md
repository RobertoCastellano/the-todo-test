## Objetivo
La idea de este proyecto es conocer un poco cómo trabajas y te desenvuelves. Un TODO es una mini app en la que básicamente uno puede añadir tareas o cosas que tiene que hacer más adelante (y visualizarlas). Un ejemplo muy popular sería: https://todo.microsoft.com/.

No es necesario que reeinventes la rueda ni que hagas un proyecto muy complejo: esto solo sirve para tener una toma de contacto. Te recomendamos dedicarle un día aproximadamente (no más de 5-6h).

## Requisitos

* Como te manejas con React.
* Como te manejas con un contendor de estado (redux o similar)
* Como te manejas con typescript.
* Sabes conectarte a una API (GET, POST etc) (te recomendamos algo como https://jsonplaceholder.typicode.com/ pero puedes usar cualquiera que te convenga)
* Puedas hacer un diseño/ maquetación básicos de la app. Te recomendamos usar una librería de componentes https://mui.com/

## Cómo hacer esta prueba
Para enviarnos esta tarea, haz fork del repositorio y sube una merge request para que le revisemos.



